MS SQL 전자정부프레임워크

**[pom.xml]**
```
<dependency>
    <groupId>com.microsoft.sqlserver</groupId>
    <artifactId>mssql-jdbc</artifactId>
    <version>8.2.2.jre11</version>
</dependency>
```

**[globals.properties]**
```
Globals.DbType = mssql
Globals.ms.DriverClassName=com.microsoft.sqlserver.jdbc.SQLServerDriver
Globals.ms.Url=jdbc:sqlserver://127.0.0.1:1433;databaseName=mssql 
Globals.ms.UserName=user
Globals.ms.Password=password
```

**[context-datasource.xml]**
```
<beans profile="mssql">
    <bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource" destroy-method="close">
        <property name="driverClassName" value="${Globals.ms.DriverClassName}"/>
        <property name="url" value="${Globals.ms.Url}" />
        <property name="username" value="${Globals.ms.UserName}"/>
        <property name="password" value="${Globals.ms.Password}"/>     
    </bean>
</beans>
```

**참고 링크**
> https://docs.microsoft.com/ko-kr/sql/linux/quickstart-install-connect-docker?view=sql-server-ver15&pivots=cs1-bash 
> https://docs.microsoft.com/ko-kr/sql/connect/jdbc/download-microsoft-jdbc-driver-for-sql-server?view=sql-server-ver15
